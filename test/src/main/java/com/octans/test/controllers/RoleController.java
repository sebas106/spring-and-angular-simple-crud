package com.octans.test.controllers;

import java.util.ArrayList;
import java.util.Optional;

import com.octans.test.models.RoleModel;
import com.octans.test.services.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    RoleService roleService;

    @GetMapping()
    public ArrayList<RoleModel> getRoles(){
        return roleService.getRoles();
    }

}