package com.octans.test.repositories;

import java.util.ArrayList;

import com.octans.test.models.RoleModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<RoleModel, Integer> {
}