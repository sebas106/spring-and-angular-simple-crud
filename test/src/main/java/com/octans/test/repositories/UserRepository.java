package com.octans.test.repositories;

import java.util.ArrayList;

import com.octans.test.models.UserModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Long> {
    public abstract ArrayList<UserModel> findByNameContaining(String name);

}