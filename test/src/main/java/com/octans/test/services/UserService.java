package com.octans.test.services;

import java.util.ArrayList;
import java.util.Optional;

import com.octans.test.models.UserModel;
import com.octans.test.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    
    public ArrayList<UserModel> getUsers(){
        return (ArrayList<UserModel>) userRepository.findAll();
    }

    public UserModel saveUser(UserModel User){
        return userRepository.save(User);
    }

    public Optional<UserModel> getById(Long id){
        return userRepository.findById(id);
    }


    public ArrayList<UserModel>  getByName(String name) {
        return userRepository.findByNameContaining(name);
    }

    public boolean deleteUser(Long id) {
        try{
            userRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }


    
}