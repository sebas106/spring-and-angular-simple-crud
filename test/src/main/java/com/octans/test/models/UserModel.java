package com.octans.test.models;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private String active;

    @ManyToOne(targetEntity = RoleModel.class)
    @JoinColumn(nullable = false)
    private RoleModel role;

    public void setRole(RoleModel role){
        this.role = role;
    }

    public RoleModel getRole(){
        return role;
    }

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getActive(){
        return active;
    }

    public void setActive(String active){
        this.active = active;
    }
    
}
