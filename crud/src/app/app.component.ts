import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'crud';

  baseURL = 'http://localhost:8080'
  userForm:FormGroup;
  isSubmitted = false;
  userList;
  roles;

  constructor(
    private _http: HttpClient,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {      
    this.userForm = this._fb.group({
      id: [0],
      name: ['', Validators.required],
      role: ['', Validators.required],
      active: ['', Validators.required],
    });

    this.getAll();
  }

  get _fc() { return this.userForm.controls; }

  save(){
    this.isSubmitted = true;
    if (this.userForm.invalid) {
        return;
    } else{
      let id = this.userForm.controls.id.value;
      if(!id){
        this._http.post(this.baseURL + '/user', this.userForm.value).subscribe(() => {
          alert('Usuario creado');
          this.reset();
        });
      } else {
        this._http.post(this.baseURL + '/user', this.userForm.value).subscribe(() => {
          alert('Usuario actualizado');
          this.reset();
        });
      }
    }
  }

  reset(){
    this.userForm.reset();
    this.isSubmitted = false;

    this.getAll();
  }

  getAll(){
    
    this._http.get(this.baseURL + '/user').subscribe((result) => {
      this.userList = result ? result : [];
    });

    this._http.get(this.baseURL + '/role').subscribe((result) => {
      this.roles = result ? result : [];
    });
  }

  edit(id){
    if(id){
      const user = this.userList.find(x => x.id === id);
        if (!user) return;
        user.isReading = true;

      this._http.get(this.baseURL + '/user/' + id).subscribe((result) => {
        Object.keys(this.userForm.controls).forEach(key => {
          this.userForm.controls[key].setValue(result[key]);
        });
        this.userForm.controls.role.setValue(user.role.id);
        user.isReading = false;
        alert('Datos del usuario a editar cargados correctamente');
      });
    }
  }

  delete(id){
    var result = confirm("Seguro que deseas eliminar al usuario?");
    if(id && result){
      const user = this.userList.find(x => x.id === id);
        if (!user) return;
        user.isDeleting = true;
        
      this._http.delete(this.baseURL + '/user/' + id).subscribe((res) => {}, 
        (err) => {
          if (err.status == 200){
            user.isReading = false;
            this.reset();
            alert('Se elimino el usuario satisfactoriamente');

          } else {
            alert('No se pudo eliminar el ususario');
          }
        });
    }
  }

  search(){
    
    this._http.get(this.baseURL + '/user/query', {
      params: {
        name: this.userForm.value.name
      }}).subscribe((result) => {
        this.userList = result ? result : [];
    });
  }
}

