package com.octans.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeyHeyPeople{

	@RequestMapping("/")
	public String hey(){
		return "Hey Hey People!";
	}
}