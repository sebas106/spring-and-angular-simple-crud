package com.octans.test.models;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class RoleModel{

    @Id
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String name;


    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    
    public RoleModel() {}

    public RoleModel(Integer id) {
        super();
        this.id = id;
    }

    
}