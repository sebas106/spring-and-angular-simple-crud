package com.octans.test.services;

import java.util.ArrayList;
import java.util.Optional;

import com.octans.test.models.RoleModel;
import com.octans.test.repositories.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;
    
    public ArrayList<RoleModel> getRoles(){
        return (ArrayList<RoleModel>) roleRepository.findAll();
    }
    
}